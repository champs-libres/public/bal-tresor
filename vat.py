# -*- coding: utf-8 -*-

from movement import UniqueMovement
from datetime import date
from decimal import *


class MonthlyVat:

    def __init__(self):
        self._amounts = {}
        self._dueDay = 20


    def registerMovement(self, movement):
        htva = movement.amount / ((100 + movement.vatRate) / 100)
        amount = Decimal((movement.amount - htva) * -1)
        if (amount == 0):
            return
        year = movement.date.year
        month = movement.date.month
        totalAmount = amount + self._amounts.get(year, {}).get(month, 0)
        if (year not in self._amounts):
            self._amounts[year] = {}
        self._amounts[year][month] = totalAmount

    def getAmounts(self):
        return self._amounts

    def getMovements(self, startPeriod, endPerio):
        amount_with_report = {}
        for year, monthlyAmount in list(self._amounts.items()):
            month = 1
            while month <= 12 :
                amount = self._amounts.get(year, {}).get(month, 0) + amount_with_report.get(year, {}).get(month, 0)
                totalAmount = amount + amount_with_report.get(year, {}).get(month, 0)
                if (year not in amount_with_report):
                    amount_with_report[year] = {}
                #amount_with_report[year][month] = 0
                
                next_month = (year, month + 1) 
                if month + 1 > 12 :
                   next_month = (year + 1, 1)
                if (next_month[0] not in amount_with_report):
                   amount_with_report[next_month[0]] = {}
                if amount < 0 :
                   totalAmount = amount + amount_with_report.get(year, {}).get(month, 0)
                   amount_with_report[year][month] = totalAmount
                elif amount > 0 and amount < 245 :
                   # on doit reporter jusqu'à être dans les conditions de paiement/remboursement
                   amount_with_report[next_month[0]][next_month[1]] = amount
                   amount_with_report[year][month] = 0
                elif amount >= 245 and amount < 615 :
                    if month == 12 :
                           amount_with_report[year][month] = amount + amount_with_report.get(year + 1, {}).get(3, 0)
                    else :
                           # on doit reporter jusqu'à être dans les conditions de paiement/REMBOURSEMENT
                           amount_with_report[next_month[0]][next_month[1]] = amount + amount_with_report.get(next_month[0], {}).get(next_month[1], 0)
                elif amount >= 615 :
                    if month == 1 or month == 2 or month == 3 :
                        amount_with_report[year][month] = amount + amount_with_report.get(year, {}).get(6, 0)
                    if month == 4 or month == 5 or month == 6 :
                        amount_with_report[year][month] = amount + amount_with_report.get(year, {}).get(9, 0)
                    if month == 7 or month == 8 or month == 9 :
                        amount_with_report[year][month] = amount + amount_with_report.get(year, {}).get(12, 0)
                    if month == 10 or month == 11 or month == 12 :
                         amount_with_report[year][month] = amount + amount_with_report.get(year + 1, {}).get(3, 0)
                month = month + 1
        movements = []
        for year, monthlyAmount in list(amount_with_report.items()):
          for month, amount in list(monthlyAmount.items()):
             dueDateT = self._defineDueDate(year, month)
             dueDate = date(dueDateT[0], dueDateT[1], dueDateT[2])
             movement = UniqueMovement(amount, dueDate, "Monthly VAT")
             movements.append(movement)
        return movements

    def _defineDueDate(self, year, month):
        if (month + 1 > 12):
            return (year + 1, 1, self._dueDay)
        else:
            return (year, month + 1, self._dueDay)
