# -*- coding: utf-8 -*-


class TreasuryPlan:

    def __init__(self, init_solde, startDate, endDate):
        self.init_solde = init_solde
        self.startDate = startDate
        self.endDate = endDate
        self.movements = []
        self._readyForExport = False

    def registerVat(self, vat):
        self.vat = vat

    def addCompoundMovement(self, movements):
        for movement in movements.getMovements(self.startDate, self.endDate):
            self.addUniqueMovement(movement)

    def addUniqueMovement(self, movement):
        self.movements.append(movement)
        self.vat.registerMovement(movement)

    def export(self):
        export_mov = []
        #add current movements
        export_mov += self.movements
        #add VAT movements
        vat_mov = self.vat.getMovements(self.startDate, self.endDate)
        if vat_mov is not None:
            export_mov += self.vat.getMovements(self.startDate, self.endDate)
        #sort by date
        export_mov.sort(key=lambda movement: movement.date)
        #create tuples with sold before, movement, sold after
        amount_before = self.init_solde
        export_list = []
        for movement in export_mov:
            line = (amount_before, movement, amount_before + movement.amount)
            export_list.append(line)
            amount_before += movement.amount
        return export_list


class PlanConsoleFormatter:

    def __init__(self, treasury_plan):
        self.treasury_plan = treasury_plan

    def export(self):
        lines = self.treasury_plan.export()
        print(("Initial solde : {0:.2f}".format(lines[0][0])))
        print(("date \t\t amount \t solde after \t\t description"))
        for line in lines:
            amount_after = line[2]
            movement = line[1]
            print((movement.date.isoformat()
                + "{0:>13.2f}".format(movement.amount)
                + "\t\t {0:>10.2f}".format(amount_after)
                + "\t\t " + movement.description.strip()))
