#!/usr/bin/python3
# -*- coding: utf-8 -*-

from plan import TreasuryPlan, PlanConsoleFormatter
from vat import MonthlyVat
from datetime import datetime, date
from movement import UniqueMovement
from movement import MonthlyMovement
from movement import LastDayOfMonthRule, IntegerDayOfMonthRule
from decimal import Decimal, InvalidOperation
import argparse
import csv
import configparser
from os.path import expanduser
home = expanduser("~")


def bind_header_with_row(row, required_headers, filename):
    """
    return a dictionnary with key as header, and column index as value
    throw an error if required_headers are missing
    """
    bind_headers = {}
    for i, column_name in enumerate(row):
        for j, header in enumerate(required_headers):
            if header == column_name:
                bind_headers[required_headers.pop(j)] = i
    # return only if all keys are present
    if len(required_headers) != 0:
        raise Exception('Those headers are missing in file {0} : {1}.'.format(
            filename, ''.join(str(h) + ' ' for h in required_headers)))
    return bind_headers


def parse_date(string):
    """
    parse a string and return a date object
    """
    try:
        date_temp = datetime.strptime(string, '%Y-%m-%d')
    except ValueError as e:
        raise ValueError('Date non valide : {} ({})'.format(string, e))
    return date(date_temp.year, date_temp.month, date_temp.day)


def parse_day_rule(day_string):
    """
    parse a string and return a day rule accordingly
    """
    if day_string == 'last':
        return LastDayOfMonthRule()
    elif day_string.isdigit():
        return IntegerDayOfMonthRule(int(day_string))
    else:
        raise Exception("'{} is an unrecognisable rule'".format(day_string))


# prepare and parse arguments
epilog = "\n\n\n\ntresor  Copyright (C) 2015 Champs-Libres " + \
    "<http://www.champs-libres.coop> \n" + \
    "This program comes with ABSOLUTELY NO WARRANTY; \n" + \
    "This is free software, and you are welcome to redistribute it \n" + \
    "under certain conditions."
parser = argparse.ArgumentParser(
    description="a simple program to plan treasury plan.",
    epilog=epilog
    )


parser.add_argument('config', help="path to configuration file",)
args = parser.parse_args()
config_path = args.config
print(config_path)

config = configparser.ConfigParser()
config.read_file(open(config_path))
values = config.defaults()


# prepare dates

try:
    date_from = parse_date(values.get('date_from'))
except Exception as e:
    print('unable to parse date_from')
    raise e

try:
    date_to = parse_date(values.get('date_to'))
except Exception as e:
    print('unable to parse date_to')
    raise e

# initialize plan
plan = TreasuryPlan(Decimal(values.get('initial')), date_from, date_to)
plan.registerVat(MonthlyVat())

# parse movement csv
# with open(args.movements) as movementCsv:
with open(values.get('movements')) as movements_csv:
    mov_reader = csv.reader(movements_csv, delimiter=",")
    first_row = True
    for row in mov_reader:
        if (first_row):
            first_row = False
            # bind header with row column
            bind_headers = bind_header_with_row(
                row,
                ['date', 'amount', 'vatRate', 'description'],
                movements_csv.name)
            continue
        dateMov = parse_date(row[bind_headers['date']])
        try:
            vatRate = Decimal(row[bind_headers['vatRate']])
        except InvalidOperation as e:
            raise ValueError('Taux de TVA non-valide : {} ({})'.format(
                row[bind_headers['vatRate']], e))
        description = row[bind_headers['description']]
        amount = Decimal(row[bind_headers['amount']])
        movement = UniqueMovement(
            amount, dateMov, description, vatRate=vatRate)
        plan.addUniqueMovement(movement)

if values.get('recurring_movement') is not None:
    with open(values.get('recurring_movement')) as recur_movements:
        recur_mov_reader = csv.reader(recur_movements, delimiter=",")

        first_row = True
        for row in recur_mov_reader:
            if (first_row):
                first_row = False
                binded_headers = bind_header_with_row(
                    row, [
                        'date_from', 'date_to', 'amount', 'vat_rate',
                        'day_rule', 'description'],
                    open(values.get('recurring_movement')).name)
                continue
            # prepare arguments for movement
            date_from = parse_date(row[binded_headers['date_from']])
            date_to = parse_date(row[binded_headers['date_to']])
            description = row[binded_headers['description']]
            amount = Decimal(row[binded_headers['amount']])
            day_rule = parse_day_rule(row[binded_headers['day_rule']])
            vat_rate = Decimal(row[binded_headers['vat_rate']])
            # create movement
            repeated = MonthlyMovement(
                day_rule, amount, date_from, date_to,
                description=description, vatRate=vat_rate)
            plan.addCompoundMovement(repeated)

formatter = PlanConsoleFormatter(plan)
formatter.export()

print(epilog)
