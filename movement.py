# -*- coding: utf-8 -*-

import calendar
from datetime import date


class UniqueMovement:

    def __init__(self, amount, date, description, vatRate=0):
        self.amount = amount
        self.date = date
        self.description = description
        self.vatRate = vatRate

    def getMovements(self):
        return [self, ]


class AbstractCompoundMovement:

    def __init__(self):
        pass

    def getMovements(self, startDate, endDate):
        pass


class MonthlyMovement(AbstractCompoundMovement):

    def __init__(self, dayRule, amount, fromDate, toDate,
        description, vatRate=0):
        super(MonthlyMovement, self).__init__()
        self.dayRule = dayRule
        self.amount = amount
        self.fromDate = fromDate
        self.toDate = toDate
        self.description = description
        self.vatRate = vatRate

    def getMovements(self, startPeriod, endPeriod):
        movements = []
        myStart = startPeriod if startPeriod > self.fromDate \
            else self.fromDate
        myStop = endPeriod if endPeriod < self.toDate \
            else self.toDate
        current = self.dayRule.get_date(myStart.year, myStart.month)
        while current <= myStop:
            movement = UniqueMovement(self.amount, current, self.description,
                vatRate=self.vatRate)
            if current >= myStart :
                movements.append(movement)
            nextYearMonth = self._getNextMonth(current)
            current = self.dayRule.get_date(nextYearMonth[0], nextYearMonth[1])
        return movements

    def _periodCovers(self, startPeriod, endPeriod):
        """
        Check that period of treasury plan covers period of Monthly
        """
        return self.fromDate < startPeriod < self.endDate \
            | self.fromDate < endPeriod < self.endDate

    def _getNextMonth(self, date):
        if (date.month == 12):
            return (date.year + 1, 1)
        else:
            return (date.year, date.month + 1)


class LastDayOfMonthRule:

    def __init__(self):
        pass

    def get_date(self, year, month):
        lastDay = calendar.monthrange(year, month)[1]
        return date(year, month, lastDay)


class IntegerDayOfMonthRule:
    """Integer day for monthly movement"""
    def __init__(self, day_integer):
        """__init__

        Keyword arguments:
        self -- self
        day_integer -- the integer of the day (must be between 1 & 28)
        """
        if day_integer < 0 or day_integer > 28:
            raise Exception('The integer day must be between 1 & 28')
        self.day_integer = day_integer

    def get_date(self, year, month):
        return date(year, month, self.day_integer)
